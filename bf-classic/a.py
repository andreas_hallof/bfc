#! /usr/bin/env python3

from hydra import WritingBloomFilter, murmur_hash


bf=WritingBloomFilter(20*10**6, 0.1, filename='test.db')

if bf.contains('hallo test'):
    print("OK schon da")
else:
    bf.add('hallo test')


