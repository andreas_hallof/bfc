#! /usr/bin/env python3

from math import ceil, log, exp

def bloom_calculator(n, p):
    """
    Calculate the optimal bloom filter parameters for a given number of elements in filter (n) and false
    positive probability (p)
    """
    m = int(ceil((n * log(p)) / log(1.0 / (pow(2.0, log(2.0))))))
    k = int(log(2.0) * m / n)
    return {'size': m, 'hashes': k}


for i in range(1,10):
    a=bloom_calculator(1219485, float(i)/float(10))
    print("Bitsize m:",a["size"], a["size"]/8.0/1024.0, "# of hashes", a["hashes"])

print()

for i in range(1,10):
    a=bloom_calculator(70*10**6, float(i)/float(10))
    print("Bitsize m:",a["size"], a["size"]/8.0/1024.0,"# of hashes", a["hashes"])

n=13*10**6
for m in [ 10*1024*1024, 12*1024*1024, 13*1024*1024, 14*1024*1024 ]:
    #print(m/float(n)*log(2))
    k=ceil(m/float(n)*log(2))
    print('"optimale" Anzahl der Hashfunktionen', k,  "Resultierende fp-rate", 
                1-exp(
                        k*n/float(m)
                     )
         )

