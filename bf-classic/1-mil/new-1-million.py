#! /usr/bin/env python3

from hydra import WritingBloomFilter, murmur_hash
import progressbar

bf=WritingBloomFilter(10**6, 0.5, filename='test.db')

MAX=10**5

widgets = [progressbar.Percentage(), progressbar.Bar()]
bar = progressbar.ProgressBar(widgets=widgets, max_value=MAX).start()
for i in range(1,MAX):
    bf.add('Zertifikat '+str(i))
    bar.update(i + 1)
bar.finish()
print()

if bf.contains('Zertifikat 1'):
    print("OK schon da")

