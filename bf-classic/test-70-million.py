#! /usr/bin/env python3

from hydra import WritingBloomFilter, murmur_hash
import progressbar

bf=WritingBloomFilter(20*10**6, 0.1, filename='test.db')

START=10*10**6
MAX=80*10**6

widgets = [progressbar.Percentage(), progressbar.Bar()]
bar = progressbar.ProgressBar(widgets=widgets, max_value=MAX).start()
fp=0
for i in range(START,MAX):
    if bf.contains('Zertifikat '+str(i)):
        fp+=1
    bar.update(i + 1)
bar.finish()

print("fp:",fp)

