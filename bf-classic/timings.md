# 8 Jahre alter Linux-PC

## Erzeugung des BF


    [a@h bfc]$ time ./new-10-million.py 
    100%|##################################################|
    OK schon da

    real    0m53,219s
    user    0m52,474s
    sys     0m0,371s

## Suche nach false-positives

    [a@h bfc]$ time ./test-70-million.py
    100%|##################################################|
    fp: 1219485

    real    6m40,286s
    user    6m39,064s
    sys     0m0,040s

# laptop

## Erzeugung des BF

    $ time ./new-10-million.py
    100%|###########################################|
    OK schon da
    
    real    0m29,390s
    user    0m28,984s
    sys     0m0,077s
    
## Suche nach false-positives

    $ time ./test-70-million.py
    100%|###########################################|
    fp: 1219485
    
    real    3m33,315s
    user    3m33,112s
    sys     0m0,046s
    
