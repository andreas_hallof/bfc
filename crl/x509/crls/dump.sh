#! /usr/bin/env bash

for i in *.crl; do
    echo $i
    openssl.exe crl -inform der -text -in $i -noout
done

