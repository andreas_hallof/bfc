#! /usr/bin/env bash

for i in *pem ; do 
    openssl x509 -text -noout -in $i;
done | grep -i 'CRL Dist' -A 10 | grep URI | sed 's/^.*URI://;' |grep '\.crl$' | sort -u

