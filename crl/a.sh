#! /usr/bin/env bash

grep '/s, .*,' crl_resolutions.csv  | sed 's/^.*\/s, //; s/,.*$//;' | \
    awk 'BEGIN { sum=0; } sum+=$1; END { print "Summe: ", sum; }'

