# Firefox Root-Zertifikate
https://ccadb-public.secure.force.com/mozilla/IncludedCACertificateReport
bzw.
https://wiki.mozilla.org/CA/Included_Certificates
und dort
https://ccadb-public.secure.force.com/mozilla/IncludedCACertificateReportPEMCSV

## Auswertung Fr, 27. Apr 2018 10:58:24

von 152 CA-Zertifikaten haben 19 eine 'X509v3 CRL Distribution Points' Angabe.
18 davon einzigartig.

## urls
https://gist.github.com/jcjones

"How to try downloading all CRLs in certificates trusted by the Mozilla Root Program"
https://gist.github.com/jcjones/92fe0297f950ba2748454b5ee8021b5b

# certstream

https://github.com/CaliDog/certstream-python

https://jsonlint.com/
ist toll

# ocsp

    [a@h crl]$ openssl ocsp -issuer LetsEncryptAuthorityX3.crt -cert wwwletsencryptorg.crt -url http://ocsp.int-x3.letsencrypt.org -respout RESP.dump
    WARNING: no nonce in response
    Response verify OK
    wwwletsencryptorg.crt: good
            This Update: May  8 18:00:00 2018 GMT
            Next Update: May 15 18:00:00 2018 GMT

