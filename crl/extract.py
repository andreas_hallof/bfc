#! /usr/bin/env python3

import csv

with open("IncludedCACertificateWithPEMReport.csv", "rt") as f:
    f_csv=csv.DictReader(f)
    anz=0
    for x in f_csv:
        anz+=1
        c=x['PEM Info']; c=c[1:-1]
        print(c)
        with open("x509/{}.pem".format(anz), "wt") as cf:
            cf.write(c)

    print(anz)

