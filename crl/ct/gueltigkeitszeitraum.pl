#! /usr/bin/env perl

use POSIX qw/ceil floor strftime/;

%a={};
$counter=0;

while (<>) {
    if (/ 'not_after'/) {
        $counter++;
        s/^.*'not_after': //;
        if (/(\d+), 'not_before': (\d+),/) {
            $a=$2; $b=$1;
            $days=int(($b-$a)/(24*60*60));
            #print( $b-$a, " ", $days ,"\n");
            $a{$days}++;
        } else {
            print "Fehler beim parsen.\n";
        }

    } else {
        #print $_;
    }
}

print($counter, " Zertifikate insgesamt im Datensatz\n");


foreach $i (sort { $a <=> $b } keys %a) {
    print $i, ": ", $a{$i},"\n" if $i>0;
}

