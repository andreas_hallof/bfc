#! /usr/bin/env python3

import sys, json, demjson, ast, re

for line in sys.stdin:
    if not line.startswith('Received messaged ->'):
        continue

    a=line.replace('Received messaged -> ','')
    a=re.sub( r"Let's Encrypt", "Let s Encrypt", a)
    a=re.sub( r'"([^"]+)\'([^"]+)\'([^"])+"', '"\1 \2 \3"', a)
    a=re.sub( r"'", '"', a)
    a=re.sub( r'None', '"None"', a)
    try:
        c=demjson.decode(a)
    except:
        print(a)
        #c=demjson.decode(a)
        #sys.exit(0)

    data=c['data']['leaf_cert']['extensions']
    if 'subjectAltName' in data:
        print(data['subjectAltName'])
    else:
        print("No subjectAltName")


