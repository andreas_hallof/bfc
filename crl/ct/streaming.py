#! /usr/bin/env python3

import certstream

def print_callback(message, context):
    print("Received messaged -> {}".format(message))

def on_open(instance):
    # Instance is the CertStreamClient instance that was opened
    print("Connection successfully established!")

def on_error(instance, exception):
    # Instance is the CertStreamClient instance that barfed
    print("Exception in CertStreamClient! -> {}".format(exception))

certstream.listen_for_events(print_callback, on_open=on_open, on_error=on_error)

