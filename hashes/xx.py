#! /usr/bin/env python3

import xxhash, sys

assert len(sys.argv)>2

modulus=int(sys.argv[1])
for a in sys.argv[2:]:
    print("{}: {}; ".format(a, xxhash.xxh32(a).intdigest() % modulus ), end='');

print()

