# 8 Jahre alter Linux-PC

    [a@h murmurhash]$ ./a.py
    1048576 465858959
    xxhash 32-bit --- 0.19518280029296875s seconds --- 5.123402259312825 GiB/s
    xxhash 64-bit --- 0.19195246696472168s seconds --- 5.209623068734963 GiB/s
    mmh3 32-bit --- 0.4644358158111572s seconds --- 2.1531500499233824 GiB/s
    mmh3 64-bit --- 0.2406609058380127s seconds --- 4.155224117177942 GiB/s
    sha256 256-bit (hashlib) --- 4.9836578369140625s seconds --- 0.20065583005979626 GiB/s
    sha256 256-bit (openssl backend) --- 5.128114461898804s seconds --- 0.19500344764725214 GiB/s

# laptop

    $ ./a.py
    1048576 465858959
    xxhash 32-bit --- 0.171600341796875s seconds --- 5.8274942201671704 GiB/s
    xxhash 64-bit --- 0.09360003471374512s seconds --- 10.683756721440089 GiB/s
    mmh3 32-bit --- 0.4212007522583008s seconds --- 2.374164800604989 GiB/s
    mmh3 64-bit --- 0.18720030784606934s seconds --- 5.341871557296145 GiB/s
    sha256 256-bit (hashlib) --- 2.6364047527313232s seconds --- 0.37930442924744273 GiB/s
    sha256 256-bit (openssl backend) --- 2.8704049587249756s seconds --- 0.3483828987127296 GiB/s

# links
"The dangerous SipHash myth", 2016-11-26
http://perl11.org/blog/seed.html

