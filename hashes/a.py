#! /usr/bin/env python3

import mmh3, time, hashlib, xxhash, csiphash

from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import hashes

MAX_ITER=2**10

a="a".encode("ascii")
for i in range(0,20):
    a=a+a
    #print(len(a))

print("Länge Testblock:",len(a), "mmh3:", mmh3.hash(a))


print("Pro Testfall", len(a)*MAX_ITER/(1024*1024*1024.0), "GiB Daten.")

# https://pypi.python.org/pypi/xxhash/
start=time.time()
for i in range(0, MAX_ITER):
    x=xxhash.xxh32(a).digest()
Zeit=time.time() - start
print("xxhash 32-bit --- {}s seconds --- {} GiB/s".format(Zeit, 1/Zeit))

start=time.time()
for i in range(0, MAX_ITER):
    x=xxhash.xxh64(a).digest()
Zeit=time.time() - start
print("xxhash 64-bit --- {}s seconds --- {} GiB/s".format(Zeit, 1/Zeit))


start=time.time()
for i in range(0, MAX_ITER):
    x=mmh3.hash(a)
Zeit=time.time() - start
print("mmh3 32-bit --- {}s seconds --- {} GiB/s".format(Zeit, 1/Zeit))

start=time.time()
for i in range(0, MAX_ITER):
    x=mmh3.hash64(a)
Zeit=time.time() - start
print("mmh3 64-bit --- {}s seconds --- {} GiB/s".format(Zeit, 1/Zeit))

start=time.time()
key=b'\x00'*16
for i in range(0, MAX_ITER):
    x=csiphash.siphash24(key, a)
Zeit=time.time() - start
print("siphash24 64-bit --- {}s seconds --- {} GiB/s".format(Zeit, 1/Zeit))

start=time.time()
for i in range(0, MAX_ITER):
    h=hashlib.sha256(a).digest()
Zeit=time.time() - start
print("sha256 256-bit (hashlib) --- {}s seconds --- {} GiB/s".format(Zeit, 1/Zeit))

start=time.time()
for i in range(0, MAX_ITER):
    digest = hashes.Hash(hashes.SHA256(), backend=default_backend())
    digest.update(a)
    h=digest.finalize()
Zeit=time.time() - start
print("sha256 256-bit (openssl backend) --- {}s seconds --- {} GiB/s".format(Zeit, 1/Zeit))


