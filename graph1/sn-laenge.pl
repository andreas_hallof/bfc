#! /usr/bin/env perl

use POSIX qw/ceil/;

%a={};
$counter=0;

while (<>) {
    if (/.*'serial_number': '([^']+)'/) {
        #print($1," ", ceil(length($1)/2.0), "\n");
        $a{length($1)}++;
        $counter++;
    }
}

print($counter, " Zertifikate insgesamt im Datensatz\n");

foreach $i (sort { $a <=> $b } keys %a) {
    print $i*4, ": ", $a{$i},"\n" if $i*4>0;
}

