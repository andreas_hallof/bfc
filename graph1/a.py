#! /usr/bin/env python3

import xxhash
import matplotlib.pyplot as plt

MeinMax=1000

res=[]
for i in range(0,MeinMax):
    res.append(xxhash.xxh32("{}".format(i)).intdigest() % MeinMax)

print(res)

plt.scatter([ x for x in range(0,MeinMax) ], res)
plt.savefig("g1.png")

