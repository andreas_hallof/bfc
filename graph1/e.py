#! /usr/bin/env python3

import xxhash
import matplotlib.pyplot as plt

MeinMax=100
maxx=50

res=[]
for i in range(0,maxx):
    res.append(xxhash.xxh32("{}".format(i)).intdigest() % MeinMax)

print(res)

plt.scatter([ x for x in range(0,maxx) ], res)
plt.savefig("g5.png")

