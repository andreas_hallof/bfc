#! /usr/bin/env python3

import xxhash
import matplotlib.pyplot as plt

MeinMax=10**6

res=[]
for i in range(0,MeinMax):
    res.append(xxhash.xxh32("{}".format(i)).intdigest() % MeinMax)

#print(res)

n, bins, patches = plt.hist(res, 20, facecolor='blue', alpha=0.75, rwidth=0.5)
#plt.grid(True)

plt.savefig("g3.png")

