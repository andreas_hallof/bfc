#! /usr/bin/env python3

import xxhash
import matplotlib.pyplot as plt

MeinMax=1000
maxx=MeinMax*10

res=[]
for i in range(0,maxx):
    res.append(xxhash.xxh32("{}".format(i)).intdigest() % MeinMax)

print(res)

plt.plot([ x for x in range(0,maxx) ], res)
plt.savefig("g4.png")

