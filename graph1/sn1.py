#! /usr/bin/env python3

import matplotlib.pyplot as plt

a='''16: 2
    20: 16
    32: 2
    52: 227
    56: 95
    60: 1447
    64: 22632
    72: 6
    76: 3
    80: 24
    88: 55
    92: 1564
    96: 11522
    112: 9
    116: 151
    120: 1594
    124: 130569
    128: 455809
    140: 2257302
    144: 7
    148: 240
    152: 12964
    156: 64
    160: 711'''
x=[]
y=[]

for i in a.split("\n"):
    b=i.split(":");
    x.append(int(b[0])>>2);
    y.append(int(b[1]));

print(x)
print(y)

plt.scatter(x, y)
plt.savefig("sn1.png")

