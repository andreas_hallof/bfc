#! /usr/bin/env python3

import xxhash, progressbar, bitarray, sys, math
from xxhash import xxh32

ratio=0.2
size_universe=10**6
size_r=int(size_universe*ratio)

print("Universum {}, davon gesperrt {}".format(size_universe, size_r))

p_1=ratio*math.sqrt(0.5)
print(math.log(1/p_1)/math.log(2))
filter_size=int(-1*size_universe*math.log(p_1)*1/(math.log(2)**2)*0.5)

print("p_1 {}, filter_size {} ({} MiB)".format(p_1, filter_size, filter_size>>20))

# https://pypi.org/project/bitarray/
bf_1=bitarray.bitarray(filter_size)
bf_1.setall(False)

widgets = [progressbar.Percentage(), progressbar.Bar()]
bar = progressbar.ProgressBar(widgets=widgets, max_value=size_r).start()
for i in range(0,size_r):
    bf_1[ xxh32("{}".format(i)).intdigest() % filter_size ]=True
    bf_1[ xxh32("A{}".format(i)).intdigest() % filter_size ]=True
    bf_1[ xxh32("B{}".format(i)).intdigest() % filter_size ]=True
    bf_1[ xxh32("C{}".format(i)).intdigest() % filter_size ]=True
    bar.update(i + 1)
bar.finish()
print()

fp=0
bar = progressbar.ProgressBar(widgets=widgets, max_value=size_universe).start()
for i in range(size_r,size_universe):
    if bf_1[xxh32("{}".format(i)).intdigest() % filter_size ] and \
       bf_1[xxh32("A{}".format(i)).intdigest() % filter_size ] and \
       bf_1[xxh32("B{}".format(i)).intdigest() % filter_size ] and \
       bf_1[xxh32("C{}".format(i)).intdigest() % filter_size ]:
        fp+=1
    bar.update(i + 1)
bar.finish()
print()

print("False Positiv {} (=> {}%)".format(fp, fp/float(size_universe)*100))
    

