#! /usr/bin/env python3

import xxhash, progressbar, bitarray, sys, math
from xxhash import xxh32


class bfc():
    def __init__(self, a):
        self.struktur=[]
        for x in a:
            assert x[0]>0
            assert x[1]>0
            b=bitarray.bitarray(x[0])
            b.setall(False)
            self.struktur.append([ b, x[1] ])

    def __str__(self):
        res=""; level=0
        for x in self.struktur:
            level+=1
            res+="Level {}, Size {}, Memsize {}, k {}, #True-s {}\n".format(
                 level, x[0].length(), x[0].buffer_info()[1], x[1], x[0].count(True)
            )

        return res

    def size(self):
        res=0
        for x in self.struktur:
            res+=x[0].buffer_info()[1]
        return res
        
    def debug(self):
        level=0
        for x in self.struktur:
            level+=1
            b, k = x[0], x[1]
            print("Level", level,"b=",b, "k=",k)
        

    def hash_i(self, i, a):
        x=xxh32(a)
        for k_loop in range(1,i):
            #print("hash_i-loop",k_loop)
            x=xxh32(x.digest())
        #print(x.intdigest())
        return x.intdigest()

    def bfno(self, level, a):
        assert level>0
        assert level<=len(self.struktur)
        b, k = self.struktur[level-1]
        for i in range(0,k):
            if not b[ self.hash_i(i, a) % b.length()]:
                return True
        return False

    def bf_insert(self, level, a):
        assert level-1<=len(self.struktur)
        b, k = self.struktur[level-1]
        #print("bf_insert: k=",k)
        for i in range(0,k):
            b[ self.hash_i(i, a) % b.length() ]=True
        return True

    def is_r(self,a):
        level=0
        for x in self.struktur:
            level+=1
            if level % 2==1:
                if self.bfno(level, a):
                    return False
            else:
                if self.bfno(level, a):
                    return True
        if len(self.struktur) % 2 == 1:
            return True
        else:
            return False

    def is_s(self,a):
        return not self.is_r(a)

    def insert_r(self, a):
        """Fügt ein Element in die bfc ein.

        Falls das Element schon enthalten war/ist (die bfc also nicht verändert wurde), gibt die Funktion False zurück.
        Anderen Falls True.
        """

        if self.is_r(a):
            return False

        level=0
        for x in self.struktur:
            level+=1
            if level % 2 == 1:
                self.bf_insert(level, a)
            else:
                if self.bfno(level, a):
                    break

        if not self.is_r(a):
            print("Kaskade zu klein für Einfügen von {} in R".format(a))

        return True

    def insert_s(self, a):

        if self.is_s(a):
            return False

        level=2
        for x in self.struktur[1:]:
            if level % 2 == 0:
                self.bf_insert(level, a)
            else:
                if self.bfno(level, a):
                    break
            level+=1

        if not self.is_s(a):
            print("Kaskade zu klein für Einfügen von {} in S".format(a))

        return True




if __name__ == '__main__':

    b=bfc([ [13, 3], [10,2], [10,1], [7,1] ])
    print(b)
    print("Size", b.size())
    start_r=0
    start_s=10
    stop_s=20

    def Element_R(i):
        return "Test {}".format(i)

    def Element_S(i):
        return "Test {}".format(i)

    def CheckElemente():
        Fehlstellen=0
        for i in range(start_r, start_s):
            if not b.is_r(Element_R(i)):
                print("R:",Element_R(i), "not in b!")
                Fehlstellen+=1

        for i in range(start_s, stop_s):
            if not b.is_s(Element_S(i)):
                print("S:",Element_R(i), "not in b!")
                Fehlstellen+=1
        print("CheckElemente: Fehlstellen={}".format(Fehlstellen))


    for i in range(start_r, start_s):
        print("inserting into R:", Element_R(i))
        b.insert_r(Element_R(i))
        b.debug();

    for i in range(start_s,stop_s):
        print("inserting into S:", Element_S(i))
        b.insert_s(Element_S(i))
        b.debug();

    print(b)
    CheckElemente()

    for i in range(0,100):
        changed=False
        for i in range(start_r,start_s): 
            if b.insert_r(Element_R(i)):
                print(Element_R(i), "needed work for R")
                changed=True

        for i in range(start_s,stop_s): 
            if b.insert_s(Element_S(i)):
                print(Element_S(i), "needed work for S")
                changed=True
        if not changed: 
            print("Keine Änderung mehr (i={}) -> break".format(i))
            break

    print(b)
    b.debug()
    CheckElemente()


    sys.exit(0)

    ratio=0.2
    size_universe=10**6
    size_r=int(size_universe*ratio)

    print("Universum {}, davon gesperrt {}".format(size_universe, size_r))

    p_1=ratio*math.sqrt(0.5)
    print(math.log(1/p_1)/math.log(2))
    filter_size=int(-1*size_universe*math.log(p_1)*1/(math.log(2)**2)*0.5)

    print("p_1 {}, filter_size {} ({} MiB)".format(p_1, filter_size, filter_size>>20))

    # https://pypi.org/project/bitarray/
    bf_1=bitarray.bitarray(filter_size)
    bf_1.setall(False)

    widgets = [progressbar.Percentage(), progressbar.Bar()]
    bar = progressbar.ProgressBar(widgets=widgets, max_value=size_r).start()
    for i in range(0,size_r):
        bf_1[ xxh32("{}".format(i)).intdigest() % filter_size ]=True
        bf_1[ xxh32("A{}".format(i)).intdigest() % filter_size ]=True
        bf_1[ xxh32("B{}".format(i)).intdigest() % filter_size ]=True
        bf_1[ xxh32("C{}".format(i)).intdigest() % filter_size ]=True
        bar.update(i + 1)
    bar.finish()
    print()

    fp=0
    bar = progressbar.ProgressBar(widgets=widgets, max_value=size_universe).start()
    for i in range(size_r,size_universe):
        if bf_1[xxh32("{}".format(i)).intdigest() % filter_size ] and \
           bf_1[xxh32("A{}".format(i)).intdigest() % filter_size ] and \
           bf_1[xxh32("B{}".format(i)).intdigest() % filter_size ] and \
           bf_1[xxh32("C{}".format(i)).intdigest() % filter_size ]:
            fp+=1
        bar.update(i + 1)
    bar.finish()
    print()

    print("False Positiv {} (=> {}%)".format(fp, fp/float(size_universe)*100))
        

